libev-perl (4.34-1) unstable; urgency=medium

  * Import upstream version 4.34.
  * Refresh fix-spelling-error.patch (offset).
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Oct 2023 02:44:28 +0200

libev-perl (4.33-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Apply multi-arch hints. + libev-perl: Add Multi-Arch: same.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 30 Nov 2022 15:51:37 +0000

libev-perl (4.33-1) unstable; urgency=medium

  * Import upstream version 4.33.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Mar 2020 19:10:12 +0100

libev-perl (4.32-1) unstable; urgency=medium

  * Import upstream version 4.32.
  * Update years of upstream and packaging copyright.
  * Refresh fix-spelling-error.patch (offset).
  * Declare compliance with Debian Policy 4.5.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 27 Jan 2020 17:37:10 +0100

libev-perl (4.31-1) unstable; urgency=medium

  * Import upstream version 4.31.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Dec 2019 15:34:23 +0100

libev-perl (4.30-1) unstable; urgency=medium

  * Team upload.

  * New upstream version 4.30
  * d/control:
    - Declare compliance with Debian Policy 4.4.1
    - Refresh build dependencies for cross builds
    - Annotate test-only build dependencies with <!nocheck>
    - Add Rules-Requires-Root field
  * d/copyright:
    - Refresh Debian Files stanza
  * d/u/metadata:
    - Add upstream metadata
  * d/watch:
    - Migrate to version 4 watch file format

 -- Nick Morrott <nickm@debian.org>  Mon, 02 Dec 2019 01:49:10 +0000

libev-perl (4.27-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 4.27
  * Update upstream copyright years and contact.
  * Fix licensing information for libev/ev.c.
  * Bump debhelper compatibility version to 12.
  * Declare compatibility with Debian Policy 4.4.0.

 -- intrigeri <intrigeri@debian.org>  Thu, 25 Jul 2019 18:01:51 +0000

libev-perl (4.25-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 4.25.
  * Fix typo in debian/copyright: Comments → Comment.
  * Refresh fix-spelling-error.patch.
  * Update years of upstream copyright.
  * Add /me to Uploaders.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.
  * Remove trailing whitespace from debian/*.
  * Set bindnow linker flag in debian/rules.
  * Install libev/Changes (as changelog.libev).

 -- gregor herrmann <gregoa@debian.org>  Tue, 25 Dec 2018 20:03:31 +0100

libev-perl (4.22-1) unstable; urgency=medium

  * Team upload.

  * debian/rules: build with -DEV_NO_ATFORK.
    Fixes segfault of Apache when EV is preloaded under Apache/mod_perl.
    Thanks to Niko Tyni for the analysis. (Closes: #805847)

  * New upstream release.
  * Refresh fix-spelling-error.patch (offset).
  * Add more spelling fixes to the patch.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Jan 2016 19:54:14 +0100

libev-perl (4.21-1) unstable; urgency=medium

  * Team upload.

  [ Axel Beckert ]
  * d/copyright: Fix link to schmorp's libecb home page. Found by DUCK.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream copyright.
  * Add new build dependency.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add another spelling fix to fix-spelling-error.patch.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Dec 2015 21:23:17 +0100

libev-perl (4.18-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Refresh fix-spelling-error.patch (offset).
  * Drop multiarch-checklib.patch. Makefile.PL has adapted to multiarch.
    Remove build dependency on libdevel-checklib-perl.
  * Harmonize BDS-* license names in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Sep 2014 20:18:40 +0200

libev-perl (4.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update patches.
    - Drop pod-errors.patch; fixed upstream.
    - Refresh fix-spelling-error.patch; offset.
  * Update upstream copyright years.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 May 2014 18:42:49 +0200

libev-perl (4.15-1) unstable; urgency=low

  * TEam upload

  * Imported Upstream version 4.15
  * use verbatim list of upstream copyright years
  * add a patch fixing a missing =encoding in libev/ev.pod
  * refresh all patches

 -- Damyan Ivanov <dmn@debian.org>  Tue, 22 Oct 2013 10:39:00 +0300

libev-perl (4.11-4) unstable; urgency=low

  * Team upload.
  * Add patch to find headers using Devel::CheckLib instead of looking for
    hard-coded path, which fails since multiarchi-ification.
    Thanks to Petr Salinger for the bug report and the test of the patch.
    (Closes: #718280)

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Aug 2013 17:30:28 +0200

libev-perl (4.11-3) unstable; urgency=low

  * Team upload.
  * Switch from cdbs to dh(1).
  * Enable epoll support in debian/rules. Thanks to Robert Norris for the
    bug report and hint. (Closes: #716928)
  * Use metacpan URLs. And update Vcs-* fields in debian/control.
  * Use debhelper 9.20120312 to get all hardening flags.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Jul 2013 18:02:28 +0200

libev-perl (4.11-2) unstable; urgency=low

  * Team upload.

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ Salvatore Bonaccorso ]
  * Add missing Depends on libcommon-sense-perl.
    Thanks to Pustovoyt Alexander <isntvoid@yandex.ru> (Closes: #698940)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 25 Jan 2013 19:31:56 +0100

libev-perl (4.11-1) unstable; urgency=low

  * New upstream release
  * Update upstream copyright years
  * Refresh patch

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 05 Feb 2012 17:19:24 +0100

libev-perl (4.10-1) unstable; urgency=low

  * New upstream release
  * Bump debhelper compat level to 9
  * Refresh patch

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 20 Jan 2012 15:30:36 +0100

libev-perl (4.03-3) unstable; urgency=low

  [ Alessandro Ghedini ]
  * Use non-ssh VCS URL (fixes vcs-field-uses-not-recommended-uri-format)
  * Make priority optional (no need to be 'extra')
  * Re-order d/control fields
  * Bump debhelper compat level to 8
  * Add perl to Build-Depends
  * Partially re-word the description
  * Use http://search.cpan.org/dist/EV/ URL in watch file
  * Add fix-spelling-error.patch
  * Rewrite debian/copyright using DEP5 format and the
    real license for the module (copyright file originally written
    by Nicholas Bamber)
  * Add myself to Uploaders and debian/copyright

  [ Dmitry E. Oboukhov ]
  * debian/rules: version is checked properly

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 22 Nov 2011 11:37:34 +0400

libev-perl (4.03-2) unstable; urgency=low

  * Created GIT-repo. Added VCS-* records into debian/control.

 -- Dmitry E. Oboukhov <unera@debian.org>  Mon, 21 Nov 2011 21:24:35 +0400

libev-perl (4.03-1) unstable; urgency=low

  * Initial release. (Closes: #497082)

 -- Dmitry E. Oboukhov <unera@debian.org>  Sat, 19 Nov 2011 01:05:06 +0400
